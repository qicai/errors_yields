import ROOT
from ctypes import c_double
import pandas as pd

inputFile_path = "/lustre/collider/jiangqimin/monoHcdi/run/oldcdi/MonoHbbTruthFramework_NtupleToHist_monoHbb_monoS_nominal_v_2211/W_nom.root"

RootFile = ROOT.TFile(inputFile_path)

def GetYieldsAndError(histNames):
  hist_number = len(histNames)
  error = c_double(-1.0)
#  if hist_number == 1:
#    hist = RootFile.Get(histNames)
  if hist_number == 2:
    hist = RootFile.Get(histNames[0])
    #yields = hist.IntegralAndError(-1,-1,error,"")
    #print(error.value*138.96)
    for i in range(1,hist_number):
      hist_tmp = RootFile.Get(histNames[i])
      hist.Add(hist_tmp)
      #yields = hist_tmp.IntegralAndError(-1,-1,error,"")
      #print(error.value*138.96)
  else:
    hist = RootFile.Get(histNames)
  hist.Scale(138.96)
  yields = hist.IntegralAndError(-1,-1,error,"")
  return yields,error.value

# Loop
all_nums = ["hist_0Lep_Wbb_2tag2pjet_150_200ptv_mBB_res_allcut_Nominal",
            "hist_0Lep_Wbb_2tag2pjet_200_350ptv_mBB_res_allcut_Nominal",
            "hist_0Lep_Wbb_2tag2pjet_350_500ptv_mBB_res_allcut_Nominal",
          "hist_0Lep_Wbb_2tag1pfatjet_0addtag_500_750ptv_mBB_mer_allcut_Nominal", 
          "hist_0Lep_Wbb_2tag1pfatjet_0addtag_750ptv_mBB_mer_allcut_Nominal", "caiqi"]
all_Errors = []
all_Yields = []
tb = pd.DataFrame(columns = ['Zbb', '2L 150_200', '2L 200_350','2L 350_500','2L 500_750','2L 750-'])

for i in range(5):
    hist_names = all_nums[i]
    Error,Yields=GetYieldsAndError(hist_names)
    all_Errors.append(Error)
    all_Yields.append(Yields)
    print(Error)
    print(Yields)

tb.loc[0] = ['Sh2211',str(all_Errors[0]) + '+-' + str(all_Yields[0]) , str(all_Errors[1]) + '+' + str(all_Yields[1]),
        str(all_Errors[2]) + '+' + str(all_Yields[2]),str(all_Errors[3]) + '+' + str(all_Yields[3]) , str(all_Errors[4]) + '+' + str(all_Yields[4])]
print(tb)



